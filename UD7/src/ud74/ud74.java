package ud74;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class ud74 {
	Scanner sc = new Scanner(System.in);
	Hashtable<String, Double> ht= new Hashtable<String, Double>();

	public ud74() {
		
		ht.put("pan", 0.95); 	ht.put("miel", 2.22);		ht.put("azucar", 1.15); 
		ht.put("leche", 1.20);	ht.put("cereales", 1.65);	ht.put("arroz", 1.75);  
		ht.put("agua", 1.23);	ht.put("gel", 1.90);		ht.put("gelatina", 1.42); 
		
	}
		private  void a�adirProd() {
		System.out.print("Nombre del producto: ");
		String clau=sc.next();
		System.out.print("*Recuerde utilizar comas en lugar de puntos* \nPrecio: ");
		Double preu=sc.nextDouble();
		ht.put(clau, preu);
		System.out.println("Producto a�adido");
		
	}
		private  void buscarProd() {
			System.out.print("Que producto busca? ");
			String prodct=sc.next();
			
			if (ht.containsKey(prodct.toLowerCase())) {
				System.out.println(prodct + " esta disponible");
			} else {
				System.out.println("No hay registro de " + prodct);
			}
			
	}public void listar() {
		Enumeration<String> prodKeyz = ht.keys();
		Enumeration<Double> precioelementos = ht.elements();
		while(prodKeyz.hasMoreElements()) {
			System.out.println(prodKeyz.nextElement()+ " a " + precioelementos.nextElement() +"") ;
		}
	}
	private void comprar() {
		// TODO Auto-generated method stub
		ArrayList<String> arrayProductos = new ArrayList<String>();
		String inProd;
		boolean salir=false, pag=true;
		int suma=0; int iva=0;
	 	double pagado=0, recibo=0, valor=0, valorIva=0;

		do {
			System.out.println("Que quiere comprar? *Para salir introduzca: salir*");
			inProd=sc.next();
			if (ht.containsKey(inProd)) { arrayProductos.add(inProd);}	
				else if (inProd.compareToIgnoreCase("salir") == 0) { 
				salir = true;
				} else { System.out.println("No hay registro de " + inProd);
			}
		} while (!salir);
		 suma=arrayProductos.size();
		
		//No he conseguido que "valor" sea igual a la suma de los precios(elementos) de la array que contiene las lista 
		System.out.println("Introduzca el precio total \n*Iva no incluido*");
		valor = sc.nextDouble();

		do { 
			System.out.println("Seleccione el IVA aplicado");
			iva = sc.nextInt();

			if (iva == 21) {
				salir=true;
			} else if (iva == 4){
				salir=true;
			} else { System.out.println("IVA aplicado incorrecto"); 
				salir=false;
			}
			
		} while (!salir); 
		
		 valorIva = valor + (valor*iva/100);
		 System.out.println("Cuanto pagara?");
		  
		do { pagado = sc.nextDouble();
			if (pagado < valorIva) { pag=true;
				} else { pag=false; System.out.println("Pago Insuficiente"); }
		} while (pag==true);		
		
		recibo = pagado - valorIva;
		
		System.out.println("Los productos seleccionados son :" + arrayProductos);
		System.out.println("El numero de articulos comprados son : " + (suma));
	 	System.out.println("El total de su compra es " + valor + "� (IVA NO INCLUIDO)");
	 	System.out.println("El total de su compra es " +  valorIva + "� (IVA del "+  iva + "% INCLUIDO)");
		System.out.println("Su cambio es " + recibo + "�");
	}
	
	public void exe() {
		int menu=0;
		
		do { System.out.println("\nSeleccione  \n"
				+ "1.Listar productos \n"
				+ "2.Buscar producto  \n"
				+ "3.Aadir producto   \n"
				+ "4.Comprar 		  \n"
				+ "5.Salir" );
			System.out.print(":"); menu=sc.nextInt();
			switch (menu) {
			case 1: listar();
				break;
			case 2: buscarProd();
				break;
			case 3:  a�adirProd(); 
				break;
			case 4:  comprar(); 
				break;
			case 5: System.out.println("Ha salido");
				break;
			default:  System.out.println("Opcion no valida");
			}
		} while (menu!=4);
	}
}
